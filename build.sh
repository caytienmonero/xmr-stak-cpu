#!/bin/bash

sudo lshw

sysctl -w vm.nr_hugepages=128

apt-get update
apt-get -qq install libmicrohttpd-dev libssl-dev cmake build-essential libhwloc-dev
apt-get -qq ca-certificates cmake curl g++ libssl-dev make 

cmake -DCMAKE_LINK_STATIC=ON -DMICROHTTPD_ENABLE=OFF -DHWLOC_ENABLE=OFF .
make install

cd bin
./xmr-stak-cpu